package ie.eightwest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.junit.Assert.*;

public class ITSeleniumTest {

    @Test
    public void testSelenium() {


        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver_win32\\chromedriver.exe");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");

        ChromeDriver driver = new ChromeDriver(chromeOptions);
        driver.get("http://168.63.36.18:8080/setests/");

        WebElement h2 = driver.findElementByTagName("h2");

        assertEquals("Home Page", h2.getText());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.id("testlink")).click();

        h2 = driver.findElementByTagName("h2");
        assertEquals("Test", h2.getText());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.id("homelink")).click();


        driver.quit();



        //String name = HelloWorld.getName();

        //assertNotNull(name);

    }
}
